#!/bin/bash

# Copyright © 2019 INRIA (Imran Sheikh)
# Apache 2.0  (http://www.apache.org/licenses/LICENSE-2.0)
#
# Based on Kaldi, Copyright 2019 © Johns Hopkins University (author: Daniel Povey)
# Apache 2.0  (http://www.apache.org/licenses/LICENSE-2.0)
# https://github.com/kaldi-asr/kaldi/blob/master/COPYING


if [ $# != 4 ]; then
    echo "USAGE: $0 <data_dir> <mdl_dir> <lang_dir> <ali_dir>"
    echo "          data_dir directory containing text wav.scp utt2spk spk2utt files."
    echo "          mdl_dir directory with the acoustic model for alignment"
    echo "          lang_dir directory for alignment"
    echo "          ali_dir directory for output alignments"
    exit
fi

NUMJOBS=28  # Note: number of jobs for Kaldi tools fixed here

datadir=`readlink -f $1`
mdldir=`readlink -f $2`
langdir=`readlink -f $3`
alidir=`readlink -f $4`

### some file checks
for x in text wav.scp utt2spk spk2utt
do
    if [ ! -f $datadir/$x ]; then
        printf "\nError: Please prepare $datadir directory to include $x.\n\n"
        exit 0
    fi
done

if [ ! -f "$mdldir/final.mdl" ]; then
    printf "\nError: Please prepare $mdldir directory with acoustic model for alignment.\n\n"
    exit 0
fi

for x in L.fst phones/align_lexicon.txt
do
    if [ ! -f $langdir/$x ]; then
        printf "\nError: Please prepare $langdir directory to include $x.\n\n"
        exit 0
    fi
done

### export environment
. ./path.sh  # set paths to binaries. Note that path.sh has hardcoded paths!
. ./cmd.sh   # This relates to the execution queue.

### make MFCC features
for x in $datadir   # the loop is not required
do
    steps/make_mfcc.sh --cmd "$train_cmd" --nj $NUMJOBS $x 
    utils/fix_data_dir.sh $x
    steps/compute_cmvn_stats.sh $x
    utils/fix_data_dir.sh $x
done

### get phone level alignmnets
mkdir $alidir/phn_ali
steps/align_si.sh --cmd "$train_cmd" --nj $NUMJOBS --retry_beam 160 $datadir $langdir $mdldir $alidir/phn_ali || exit 1;    # normally kaldi uses retry_beam=40
                                                                                                                             # our choice of 160 is to ensure most utterances can be forced aligned
                                                                                                                             # leaving out ones which have problems with ground truth text/wav

for i in  $alidir/phn_ali/ali.*.gz;
do
    ali-to-phones --ctm-output $mdldir/final.mdl ark:"gunzip -c $i|" - | utils/int2sym.pl -f 5 $langdir/phones.txt > ${i%.gz}.ctm;
done;

### convert phone level alignments into word level alignments
mkdir $alidir/wrd_ali_tmp
perl local/phn_to_wrd_ali.pl $alidir/phn_ali/ $langdir/phones/align_lexicon.txt $alidir/wrd_ali_tmp/
mkdir $alidir/wrd_ali
perl local/fix_wrd_ali.pl $alidir/wrd_ali_tmp/ $datadir/text $alidir/wrd_ali/



