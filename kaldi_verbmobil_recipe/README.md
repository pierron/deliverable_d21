This is a [Kaldi](http://kaldi-asr.org) recipe for training speech recognition models on the English subset of the [Verbmobil corpus](https://www.phonetik.uni-muenchen.de/Bas/BasVM1eng.html). The objective to is to train Kaldi's [nnet3](http://kaldi-asr.org/doc/dnn3.html) and [chain](https://kaldi-asr.org/doc/chain.html) models.

----

## Table of Contents
* [Prerequisites](#prerequisites)
* [Setup](#setup)
* [Running the recipe](#running-the-recipe)
  - [Data Preparation](#data-preparation)
  - [Train GMM HMM models](#train-hmm-hmm-models)
  - [Train nnet3 models](#train-nnet3-models)
* [Additional Notes](#additional-notes)
  - [Corpus and Data](#corpus-and-data)
  - [train-dev-test split](#train-dev-test-split)
  - [Problem with speaker ids](#problem-with-speaker-ids)
  - [WER with tri3 and nnet3 models](#wer-with-tri3-and-nnet3-models)

----

## Prerequisites
- This recipe will re-use binaries and scripts from the Kaldi toolkit. So you should have Kaldi pre-installed on your system.
- It requires you to install the [kaldi_lm](http://www.danielpovey.com/files/kaldi/kaldi_lm.tar.gz) tool. You can  install this tool with the *tools/extras/install\_kaldi_lm.sh* script in your Kaldi installation.
- A dump of the BAS edition of the English dialogs in [Verbmobil I](https://www.phonetik.uni-muenchen.de/Bas/BasVM1eng.html) corpus and that of the English dialogs in [Verbmobil II](https://www.phonetik.uni-muenchen.de/Bas/BasVM2eng.html). This implies CDs 6, 8 and 13 for Verbmobil I, and CDs 23, 28, 31, 32, 42, 43, 47, 50, 51, 52, 55, 56 for Verbmobil II. 

## Setup
- Ensure that you have a working Kaldi installation.
- Modify the softlinks *steps/* and *utils/* in this directory to point to *egs/wsj/s5/steps/* and *egs/wsj/s5/utils/*, respectively, in your Kaldi installation.
- Modify the path of *KALDI\_ROOT* and modify (or remove) the path to *kaldi\_lm* and *sox* tools in [path.sh](path.sh)
- Modify [cmd.sh](cmd.sh) if you are using a different execution queue for Kaldi.

## Running the recipe
The Kaldi Verbmobil English recipe has currently three main stages:
1. [Data Preparation](#data-preparation): To process the original BAS edition of the corpus and prepare data for following stages.
2. [Train GMM HMM models](#train-gmm-hmm-models): To train the initial GMM-HMM models on the corpus.
3. [Train nnet3 models](#train-nnet3-models): To train nnet3 chain models using the initial GMM-HMM models.

### Data Preparation
This stage processes the original BAS edition of the corpus to prepare data for following training stages. The script for data preparation is run as:

>`bash prepare_data.sh <trl_dir>`

	<trl_dir> is the parent directory containing all *.trl* transcriptions (for all dialogs in all Verbmobil CDs)

After running the  data preparation script above please ensure that:
- *data/local/dict/lexicon.txt.orig* lexicon file does not have missing pronunciations. Search for '<add_manually>' tags and add the missing pronunciations manually. You can also use the [online CMU Lexicon Tool](http://www.speech.cs.cmu.edu/tools/lextool.html) to generate these pronunciations. This step would be ideally replaced by an [automatic G2P tool](https://www-i6.informatik.rwth-aachen.de/web/Software/g2p.html) in the future.
- *data/local/dict/lexicon.txt.orig* file, whether modified or not, is moved/copied to *data/local/dict/lexicon.txt*.
-  contents of *data/local/dict/*, including files *optional\_silence.txt extra\_questions.txt nonsilence\_phones.txt silence_phones.txt*, are verified.

### Train GMM HMM models
This stage trains the initial GMM-HMM (tri-phone) models on the Verbmobil corpus. The training script is launched as:

>`bash train_tri3.sh`

Note that the script [train_tri3.sh](train_tri3.sh) has some pre-defined flags and constants including:
- flags to manage different stages in training tri-phone models
- constants defining number of jobs to be used in feature extraction, training and decode
- constants for tri-phone model parameters
- constants for the subset of data used for training the initial mono-phone and tri-phone models
- paths and prefixes to the directory containing Verbmobil signal files

### Train nnet3 models

After training the initial GMM-HMM (tri-phone) models on the Verbmobil corpus you can go training an [nnet3 TDNN](http://kaldi-asr.org/doc/dnn3.html) model or an [nnet3 TDNN Chain](https://kaldi-asr.org/doc/chain.html) model.

An nnet3 TDNN model can be trained using:

> `bash local/nnet3/run_tdnn_1a.sh`

Instead of the above nnet3 TDNN model, one can train an nnet3 TDNN Chain model using:

> `bash local/chain/run_tdnn.sh`

### Evaluation scripts

- *test_tr3.sh* to decode the test set with tri3 GMM HMM model
- *local/nnet3/test_nnet3.sh* to decode the test set with nnet3 model
- *local/chain/test_chain.sh* to decode the test set with nnet3 chain model
- *local/gen_ctm.sh* to generate [CTM](http://www1.icsi.berkeley.edu/Speech/docs/sctk-1.2/infmts.htm#ctm_fmt_name_0) output from a decode directory
- *local/ctm2txt.pl* to generate ASR transcripts from CTM output
- *local/analyse-wer.sh* and *analyse-wer-VNC-ADB.sh* to calcuate WER performance of a particular model, as shown in [WER table below](#wer-with-tri3-and-nnet3-models)

Note: Evaluation scripts are to be run individually and may require creating the expected output directories.

## Additional Notes

### Corpus and Data
- Currently the recipe has been tested on the English subset of Verbmobil corpus in the dump at */talc/multispeech/corpus/dialog/Verbmobil/Verbmobil* on Nancy site of Grid5000.
- sample *data/* and *exp/* directories and associated log files are available at */talc3/multispeech/calcul/users/isheikh/exp/vm-recipe/* on Nancy site of Grid5000.

### train-dev-test split
The current train-dev-test split has 92 dialogs in test, 21 dialogs in dev and 698 dialogs in train, ensuring that a new dialog domain and German English speakers are seen in dev and test, without any dev/test speakers seen in train. This splitting is implemented in the [local/getTrainDevTestSplits.pl](local/getTrainDevTestSplits.pl) script.  Based on the [corpus stats](https://drive.google.com/file/d/13B15E1GL_j02Wd6az6emn6QpW_Xhl0yX/view), it uses the following approach to decide the split:
  - All dialogs in *CDs 47, 52, 55, 56* (including *info desk* domain + German English speakers) and dialogs *q010-q020* from *CD 6* (including German English speakers) are put into the test set. Additonally, dialogs from other CDs including the speakers from test set are also put into the test set.
  - Dialogs in *CDs 32, 51* (including *info desk* domain + German English speakers) and dialogs *q001-q009 q021 q0022* from *CD 6* (including German English speakers), which are not part of the test set, are put into the dev set. Additonally, dialogs from other CDs including the speakers from dev set are also put into the dev set. Moreover, to ensure a balance in the WERs of test and dev dailogs corresponding to speakers *HCF QZO QZB* are moved from test to dev.
  - All remaining dialogs are kept as the train set. Except for dialogs of speakers *ADB* (British English speaker) and *VNC* (with Asian accented English), which are put into test and dev, respectively.

### Problem with speaker ids
As highlighted in the [corpus stats](https://drive.google.com/file/d/13B15E1GL_j02Wd6az6emn6QpW_Xhl0yX/view), speaker ids used in the Verbmobil coprus are not unique. After manually listening to dialogs, it was identified that 
  - speakers ids *CAC JDH NKH RGM* represent different speakers in Verbmobil parts I and II
  - speaker id *MAS* represents two different speakers within Verbmobil part I
To address this issue,
  - speakers ids *CAC JDH NKH RGM* in Verbmobil parts I and II are suffixed with *VM1* and *VM2*, respectively
  - speaker id *MAS* for dialog *q007n* in Verbmobil part I is suffixed with *VMZ*
  - all other speaker ids are suffixed with *VMX*

### WER with tri3 and nnet3 models
WER obtained for train-dev-test split discussed above:

|               |   EN+DE spkrs |  EN spkrs     |     DE spkrs  |    Domain A   |    Domain B   |    A n EN      |
|---------------|:-------------:|:-------------:|:-------------:|:-------------:|:-------------:|:--------------:|
| dev # wrd     |         18480 |         12706 |          5774 |         12861 |          5619 |          7087  |
| dev # sent    |          1113 |           784 |           329 |           794 |           319 |           465  |
| tri3 dev si   |         35.84 |         31.50 |         45.38 |         34.33 |         39.30 |         25.33  |
| tri3 dev      |         31.68 |         26.87 |         42.28 |         30.57 |         34.24 |         21.02  |
| nnet3 dev     |         26.16 |         21.83 |         35.69 |         25.58 |         27.50 |         17.34  |
| chain dev     |         21.87 |         17.39 |         31.73 |         22.34 |         20.79 |         14.69  |
| test #wrd     |         36222 |         28885 |          7337 |         23199 |         13023 |         15862  |
| test #sent    |          2286 |          1971 |           315 |          1383 |           903 |          1068  |
| tri3 test si  |         34.71 |         31.54 |         47.17 |         35.26 |         33.72 |         29.75  |
| tri3 test     |         28.86 |         25.63 |         41.57 |         29.86 |         27.08 |         24.44  |
| nnet3 test    |         25.45 |         22.93 |         35.37 |         25.71 |         24.99 |         21.24  |
| chain test    |         22.36 |         20.03 |         31.54 |         22.45 |         22.19 |         18.25  |

where 'EN' and 'DE' denote American and German English speakers, 'DA' and 'DB' denote *appointment scheduling* and *info desk* dialog domains, respectively, as per the corpus annotation. 'si' denotes speaker independent decoding.

WER obtained for train-dev-test split discussed above and also separting out the accented speakers, *ADB* (British English speaker) and *VNC* (with Asian accented English), into the 'DE' group:

|               |  EN+De spkrs  |  EN- spkrs    |  DE+ spkrs.   |     DA n EN-   |
|---------------|:-------------:|:-------------:|:-------------:|:--------------:|
| dev # wrd     |         18480 |         12049 |          6431 |          6430  |
| dev # sent    |          1113 |           706 |           407 |           387  |
| tri3 dev si   |         35.84 |         32.04 |         42.95 |         25.71  |
| tri3 dev      |         31.68 |         27.26 |         39.96 |         21.17  |
| nnet3 dev     |         26.16 |         22.09 |         33.79 |         17.37  |
| chain dev     |         21.87 |         17.45 |         30.14 |         14.54  |
| test #wrd     |         36222 |         26621 |          9601 |         13598  |
| test #sent    |          2286 |          1817 |           469 |           914  |
| tri3 test si  |         34.71 |         30.51 |         46.36 |         27.42  |
| tri3 test     |         28.86 |         24.60 |         40.66 |         22.23  |
| nnet3 test    |         25.45 |         22.28 |         34.25 |         19.68  |
| chain test    |         22.36 |         19.45 |         30.42 |         16.83  |
