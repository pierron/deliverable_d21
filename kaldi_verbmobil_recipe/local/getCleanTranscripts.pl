#!/usr/bin/perl

# Copyright © 2019 INRIA (Imran Sheikh)
# Apache 2.0  (http://www.apache.org/licenses/LICENSE-2.0)

if($#ARGV != 0){
	print STDERR "USAGE: $0 <trl_parent_directory>\n";
	exit(0);
}

use File::Basename;

my @vm1cds = qw/6 8 13/;
my @vm2cds = qw/23 28 31 32 42 43 47 50 51 52 55 56/;
my @dirs = ($ARGV[0]);
 
while (@dirs) {
	my $thisdir = shift @dirs;
	opendir my $dh, $thisdir;
	while (my $entry = readdir $dh) {
		next if $entry eq '.';
		next if $entry eq '..';
		
		my $fullname = "$thisdir/$entry";
				
		if (-d $fullname) {
			push @dirs, $fullname;
		}elsif($fullname =~ m/\/tr2\/.*\.trl$/){
			for my $cd (@vm1cds){
				if($fullname =~ m/VM$cd\.1\/tr2\/.+\.trl$/){
					my @turns = getTurns($fullname);
					my $cdid = sprintf("%02d", $cd);
					foreach my $turn (@turns){
						my ($id, $txt) = cleanTurn($turn);
						print "VM1_".$id."_".$cdid." ".$txt, "\n";
					}
				}
			}
		}elsif($fullname =~ m/\/trl\/.*\.trl$/){
			for my $cd (@vm2cds){
				if($fullname =~ m/VM$cd\.1\/trl\/.+\.trl$/){
					my @turns = getTurns($fullname);
					my $cdid = sprintf("%02d", $cd);
					foreach my $turn (@turns){
						my ($id, $txt) = cleanTurn($turn);
						print "VM2_".$id."_".$cdid." ".$txt, "\n";
					}
				}
			}
		}
	}
}


##### Initial Reference was https://www.phonetik.uni-muenchen.de/forschung/Verbmobil/VMtrlex2d.html
##### Update with rules from https://www.phonetik.uni-muenchen.de/forschung/Verbmobil/trllex_e_html/projects/verbmobil.html


##### Turn level markups
### Keep turn beginning with "<*tENG> ", ignore turns beginning with "<*GER> ". Note: In Verbmobil English subset "<*tENG> " is used in the multilingual CDs. 		  In Verbmobil 1 English subset "<*GER>" (without following space) is used to indicate German word. 
### ";" in beginning of line indicates comments
### one turns can be written in multiple line but turns separated by empty line
sub getTurns(){
	my $filename = $_[0];
	my @turnarray;
	my $convid = basename($filename);
	$convid =~ s/\.trl$//;
	my $turntxt = "";
	open my $FH, "<", $filename or die "can't read open '$filename': $OS_ERROR";
	while (my $line = <$FH>) {
		$line =~ s/\s+$//;
		$line =~ s/^\s+//;
		if($line =~ m/^\;/){
			next;
		}elsif($line =~ m/^$/){
			next;
		}elsif($line =~ m/^$convid/){
			if($turntxt !~ m/^$/){
				if($turntxt !~ m/\:\s*\<\*tGER\>/){
					$turntxt =~ s/\:\s*\<\*tENG\>/: /;
					push @turnarray, $turntxt."\n";
				}
			}
			$turntxt = $line;
		}else{
			$turntxt .= " ".$line;
		}
	}	
	if($turntxt !~ m/^$/){
		if($turntxt !~ m/\:\s*\<\*tGER\>/){
			$turntxt =~ s/\:\s*\<\*tENG\>/: /;
			push @turnarray, $turntxt."\n";
		}
	}
	return @turnarray;
}

sub cleanTurn(){
	my $turn = $_[0];
	$turn =~ s/\s+$//;
	$turn =~ s/^\s+//;
	my ($id, $txt) = split(/\:/, $turn, 2);	
	$txt = " $txt ";

### "+/" in begin of word/phrase and "/+" in the end indicates repition/correction. Not all of them should be treated as OOV! (Replace with OOV tag at the lat stage based on word frequency?)
	$txt =~ s/ \+\// /g;
	$txt =~ s/\/\+ / /g;
	#print "----",$txt,"\n";

### "-/" at begin of word/phrase and "/-" at the end indicate false start 
	$txt =~ s/ \-\// /g;	# keep <\/s> <s> ?
	$txt =~ s/\/\- / /g;	# keep <\/s> <s> ?

##### Overlay regions
### <:<XXX> ABCD:> indicates <XXX> sound/noise overlay on ABCD. Note other symbols are allowed in ABCD.
### @ speaker overlays in Verbmbil 2 to be handled!
	$txt =~ s/\<\:\<(.*?)\>/ /g;	
	$txt =~ s/\:\> / /g;
	#print "----",$txt,"\n";

##### Replace by nothing group
### @n nth indicate active  interference
### n@ nth indicate passive interference
	$txt =~ s/\<?\@\d+/ /g;		
	$txt =~ s/\d+\@\>?/ /g;	
### "<; XXXXX>" indicates local comments
### "<P>" indicates pause
### "<L>" indicates lenghthened sound
### " ~" preceeding units indicates name
### " #" preceeding units indicates number
### "_ " at end of word indicates the word is interrupted, but not aborted, and " _" precedding the next word used to resume the word. In Verbmobil English susbset they can be safely replaced by nothing but other sets may be not!
	$txt =~ s/\<\;(.*?)\>/ /g;
	$txt =~ s/\<P\>/ /g;	
	$txt =~ s/\<L\>//g;
	$txt =~ s/ \~/ /g;	
	$txt =~ s/ \#/ /g;		
	$txt =~ s/ \_/ /g;	
	$txt =~ s/\_ / /g;  
	#print "----",$txt,"\n";

##### Nonverbal sounds and Noise group (in English subset)
### <"ah> <"ahm> <hm> indicate the respective hesitations
### <h"as> <hes> indicate other hesitations
### <Cough> <Laugh> <Smack> <Swallow> <Throat> <Schmatzen> are non verbal sounds
### <Noise> are other non verbal sounds
### <A> <B> indicates breathe
### <#Klicken> <#Knock> <#Rustle> <#Squeak> indicate technical sounds
### <#> indicate noises other than <#Klicken> <#Knock> <#Rustle> <#Squeak> categories
	#$txt =~ s/\<(\"ah|ahm|hm|h\"as|uh|uhm)\>/[hesitation]/g;	# do not map to one group
	$txt =~ s/\<(\"ah|uh)\>/ahh/g;
	$txt =~ s/\<(ahm|hm|uhm)\>/umm/g;
	$txt =~ s/\<hes\>/[hes]/g;	
	$txt =~ s/\<h\"as\>/[hes]/g;	# does not occur in VM1

	$txt =~ s/\<(A|B|Cough|Laugh|Smack|Swallow|Throat|Schmatzen)\>/[noise1]/g;
	$txt =~ s/\<Noise\>/[noise1]/g;	
	$txt =~ s/\<\#(Klicken|Knock|Rustle|Squeak)\>/[noise2]/g;	
	$txt =~ s/\<\#\>/[noise2]/g;	
	#print "----",$txt,"\n";

##### unk group?
### " *" preceeding units indicates neologism (non-existing words)
### "<%>" indicates incomprehensible voice productions
### "<*XXX>" preceeding word means foreign language word
### "% " at end of word indicates the word is difficult to understand in audio
### "= " at end of word indicates the word was aborted while speaking
### "<T_>" in the begin of word and "<_T>" in the end of word indicate a technical abort of word. In Verbmobil English susbset they are very few can be considered as OOV?
	$txt =~ s/ \*(.*?) / <unk> /g;
	$txt =~ s/\<\%\>/ <unk> /g;	
	$txt =~ s/\<\*[A-Z]+\>(.*?) /<unk> /g;
	$txt =~ s/ (?:(?! ).)*?\% / <unk> /g;	# uses negative lookahead assertion
	$txt =~ s/ (?:(?! ).)*?\= / <unk> /g;	# uses negative lookahead assertion
	$txt =~ s/ \<T\_\>(.*?) / <unk> /g;
	$txt =~ s/ (?:(?! ).)*?\<\_T\> / <unk> /g;	# uses negative lookahead assertion

	# to handle consecutive <unk>. Note: not a good solution to do it twice!
	$txt =~ s/ \*(.*?) / <unk> /g;
	$txt =~ s/ (?:(?! ).)*?\% / <unk> /g;	
	$txt =~ s/ (?:(?! ).)*?\= / <unk> /g;	
	#print "----",$txt,"\n";

##### <s> </s> group
### "<*T>" and "<*T>t" are technical turn off in middle and end of turn, respectively.
### " . " " , " " ? " are punctuation marks (Ignore ","?)
	$txt =~ s/\.\s*$/ /g;	# keep <\/s> ?
	$txt =~ s/\?\s*$//g;	# keep <\/s> ?
	$txt =~ s/\./ /g;		# keep <\/s> <s> ?
	$txt =~ s/\?/ /g;		# keep <\/s> <s> ?
	$txt =~ s/\,/ /g;

### <! n comment> indicates a comment on the pronunciation of n words before. In Verbmobil English susbset they can be handled by alternate pronunciations? Also note 
# grep -R -P "\<\!\d" trs/ | grep -v -P "\'(m|ve|ll|d|re|s|bout|til|cause|kay)\>" | grep -v -P "\<\!\d$" | grep -v -P "(nna|tta|an\'|kinda|kay|yah|lemme|dunno)\>" | grep -v "GER>"
# in which "\'(m|ve|ll|d|re|s|bout|til|cause)\>" should replace previous token and also
# "(nna|tta|kinda|lemme|dunno)\>". 
# Note previous n words can have other tags!
	my @wrds = split(/\s+/, $txt);
	my $closeFlag=0;
	my $cnt;
	my $cnt2=0;
	for(my $wid=0; $wid <= $#wrds; $wid++){
		if($wrds[$wid] =~ m/\<\!\d+/){
			$cnt = $wrds[$wid];
			$cnt =~ s/\<\!//;
			$closeFlag = 1;
			$cnt2=1;
		}elsif($closeFlag && ($wrds[$wid] =~ m/\>/)){
			if(($wrds[$wid] =~ m/\'(m|ve|ll|d|re|s|bout|til|cause)\>/) || ($wrds[$wid] =~ m/(nna|tta|kinda|lemme|dunno)\>/)){
				for(my $j=$cnt+$cnt2; $j>=$cnt2; $j--){
					$wrds[$wid-$j] = "";
				}
				$wrds[$wid] =~ s/\>//;
			}else{
				for(my $j=$cnt2; $j>=0; $j--){
					$wrds[$wid-$j] = "";
				}
			}
			$closeFlag = 0;
		}elsif($closeFlag && ($wrds[$wid] !~ m/\>/)){
			$cnt2 +=1;
		}
	}
	$txt = join(" ",@wrds);
	$txt =~ s/^\s*//;

### "$" preceeding a single char indicates spelled out (as in abbreviation). Note " " or "-" can preceede "$"
	my @tokens = split(/\s+/, $txt);
	foreach my $i (0 .. $#tokens){
		if($tokens[$i] =~ m/\$/){
			my @tmp = split(/\$/, $tokens[$i]);
			$tokens[$i] = join(".",@tmp).".";
			$tokens[$i] =~ s/^\.//;
		}
	}
	$txt = join(" ",@tokens);
	$txt =~ s/^\s*//;
	$txt =~ s/\-\./._/g;	

### "-", without a space before or after, indicates compound word. (seperate these tokens?)
	$txt =~ s/\-/ /g;			

##### [Following are not observed in the Verbmobil English susbset]
### "´" which indicates word reduction?
### "<Z>" which indicates delay?
### "!KEY!" which indicates some keyword (comments)
### "<PP>" which indicate long pauses due to recording scenario

### etc
	#$txt =~ s/(\<unk\>\s+)+/<unk> /g;
	#$txt =~ s/\'/ '/g;

### following appear due to alternae pronunciations marked by annotator, and are being replaced to avoid unnecessary enteries in lexicon
	$txt =~ s/(\s|^)\'et\'s / let's /g;
	$txt =~ s/(\s|^)\'m / am /g;
	$txt =~ s/(\s|^)\'ll / will /g;
	$txt =~ s/(\s|^)\'til / till /g;
	$txt =~ s/(\s|^)\'till / till /g;
	# 's 'd  't's resolve into multiple terms

	$txt =~ s/^\s+//;
	$txt =~ s/\s+$//;
	$txt =~ s/\s+/ /g;

	$txt = lc($txt);

	return ($id, $txt);
}



