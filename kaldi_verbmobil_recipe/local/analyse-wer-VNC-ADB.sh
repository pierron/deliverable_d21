#!/bin/bash

# Copyright © 2019 INRIA (Imran Sheikh)
# Apache 2.0  (http://www.apache.org/licenses/LICENSE-2.0)

. ./path.sh  # set paths to binaries

set -e

devRef="data/split/dev/text"
testRef="data/split/test/text"

tri3siDevHyp=exp/tri3_full/decode_dev.si/ctm_12_1.0/dev.hyp
tri3siTestHyp=exp/tri3_full/decode_test.si/ctm_12_1.0/test.hyp
tri3DevHyp=exp/tri3_full/decode_dev/ctm_12_1.0/dev.hyp
tri3TestHyp=exp/tri3_full/decode_test/ctm_12_1.0/test.hyp
nnet3DevHyp="exp/nnet3/tdnn1a_sp/decode_split_dev/ctm_12_1.0/dev.hyp"
nnet3TestHyp="exp/nnet3/tdnn1a_sp/decode_split_test/ctm_12_1.0/test.hyp"
chainDevHyp="exp/chain/tdnn1f_sp/decode_tg_split_dev/ctm_10_0.0/dev.hyp"
chainTestHyp="exp/chain/tdnn1f_sp/decode_tg_split_test/ctm_10_0.0/test.hyp"

modelSuffix="chain"
cleanTranscripts=1

if [ $modelSuffix == "chain" ]; then
	devHyp=$chainDevHyp 
	testHyp=$chainTestHyp 
elif [ $modelSuffix == "nnet3" ]; then
	devHyp=$nnet3DevHyp 
	testHyp=$nnet3TestHyp
elif [ $modelSuffix == "tri3" ]; then
	devHyp=$tri3DevHyp 
	testHyp=$tri3TestHyp
elif [ $modelSuffix == "tri3" ]; then
	devHyp=$tri3siDevHyp 
	testHyp=$tri3siTestHyp
else
	echo "Error modelSuffix is $modelSuffix"
	exit
fi	

c1="1010101"
c2="1010101"
c3="1010101"
cleanSuffix="all"
outdir='tmp/trial5_full/wer_analysis/'$modelSuffix'_all/'
if [ $cleanTranscripts -eq 1 ]; then
	c1="\[noise[1-4]\]"
	c2="<unk>"
	c3="\[hes\]"
	cleanSuffix="sp"
	outdir='tmp/trial5_full/wer_analysis/'$modelSuffix'_sp/'
fi	

declare -A matrix

i=0
wer=""
wrdCnt=""
sentCnt=""
get_wer () {
	cat /tmp/wer
	wer=`cat /tmp/wer | grep WER | cut -d " " -f2`
	wrdCnt=`cat /tmp/wer | grep WER | cut -d " " -f6 | cut -d "," -f1`
	sentCnt=`cat /tmp/wer | grep SER | cut -d " " -f6 | cut -d "," -f1`
}

echo "======================dev=================================="
###------------WER on dialogs with German Accented (q0) versus other (non-q0) speakers------------###
echo "------------q0-ADB-VNC------------"
cat $devRef | sed "s/$c1//g" | sed "s/$c2//g" | sed "s/$c3//g" | egrep "(_q0|VNCVMX|ADBVMX)" > $outdir/dev.q0-ADB-VNC.$cleanSuffix.ref
cat $devRef | sed "s/$c1//g" | sed "s/$c2//g" | sed "s/$c3//g" | egrep -v "(_q0|VNCVMX|ADBVMX)" > $outdir/dev.non-q0-ADB-VNC.$cleanSuffix.ref
cat $outdir/dev.q0-ADB-VNC.$cleanSuffix.ref $outdir/dev.non-q0-ADB-VNC.$cleanSuffix.ref > $outdir/dev.$cleanSuffix.ref

cat $devHyp | sed "s/$c1//g" | sed "s/$c2//g" | sed "s/$c3//g" | egrep "(_q0|VNCVMX|ADBVMX)" > $outdir/$modelSuffix.dev.q0-ADB-VNC.$cleanSuffix.hyp
cat $devHyp | sed "s/$c1//g" | sed "s/$c2//g" | sed "s/$c3//g" | egrep -v "(_q0|VNCVMX|ADBVMX)" > $outdir/$modelSuffix.dev.non-q0-ADB-VNC.$cleanSuffix.hyp
cat $outdir/$modelSuffix.dev.q0-ADB-VNC.$cleanSuffix.hyp $outdir/$modelSuffix.dev.non-q0-ADB-VNC.$cleanSuffix.hyp > $outdir/$modelSuffix.dev.$cleanSuffix.hyp
compute-wer --text --mode=present ark:$outdir/dev.$cleanSuffix.ref ark:$outdir/$modelSuffix.dev.$cleanSuffix.hyp > /tmp/wer
get_wer ; ((i=+1)) ; matrix[3,$i]=$wer; matrix[1,$i]=$wrdCnt; matrix[2,$i]=$sentCnt;
compute-wer --text --mode=present ark:$outdir/dev.non-q0-ADB-VNC.$cleanSuffix.ref ark:$outdir/$modelSuffix.dev.non-q0-ADB-VNC.$cleanSuffix.hyp > /tmp/wer
get_wer ; ((i+=1)) ; matrix[3,$i]=$wer; matrix[1,$i]=$wrdCnt; matrix[2,$i]=$sentCnt;
compute-wer --text --mode=present ark:$outdir/dev.q0-ADB-VNC.$cleanSuffix.ref ark:$outdir/$modelSuffix.dev.q0-ADB-VNC.$cleanSuffix.hyp > /tmp/wer
get_wer ; ((i+=1)) ; matrix[3,$i]=$wer; matrix[1,$i]=$wrdCnt; matrix[2,$i]=$sentCnt;

###------------WER on dialogs with new dialog without q0,ADB,VNC and b------------###
echo "-------non-b-q0-ADB-VNC---------"
cat $devRef | sed "s/$c1//g" | sed "s/$c2//g" | sed "s/$c3//g" | egrep -v "m[0-9]+b" | egrep -v "(_q0|VNCVMX|ADBVMX)" > $outdir/dev.non-b-q0-ADB-VNC.$cleanSuffix.ref

cat $devHyp | sed "s/$c1//g" | sed "s/$c2//g" | sed "s/$c3//g" | egrep -v "m[0-9]+b" | egrep -v "(_q0|VNCVMX|ADBVMX)" > $outdir/$modelSuffix.dev.non-b-q0-ADB-VNC.$cleanSuffix.hyp
compute-wer --text --mode=present ark:$outdir/dev.non-b-q0-ADB-VNC.$cleanSuffix.ref ark:$outdir/$modelSuffix.dev.non-b-q0-ADB-VNC.$cleanSuffix.hyp > /tmp/wer
get_wer; ((i+=1)); matrix[3,$i]=$wer; matrix[1,$i]=$wrdCnt; matrix[2,$i]=$sentCnt;

echo ""
num_rows=3
num_columns=4
f2="|%14s "
for ((i=1;i<=num_rows;i++)) do
	for ((j=1;j<=num_columns;j++)) do
        printf "$f2" ${matrix[$i,$j]}
    done
    echo " |"
done

i=0
echo "======================test=================================="
###------------WER on dialogs with German Accented (q0) versus other (non-q0) speakers------------###
echo "------------q0-ADB-VNC------------"
cat $testRef | sed "s/$c1//g" | sed "s/$c2//g" | sed "s/$c3//g" | egrep "(_q0|VNCVMX|ADBVMX)" > $outdir/test.q0-ADB-VNC.$cleanSuffix.ref
cat $testRef | sed "s/$c1//g" | sed "s/$c2//g" | sed "s/$c3//g" | egrep -v "(_q0|VNCVMX|ADBVMX)" > $outdir/test.non-q0-ADB-VNC.$cleanSuffix.ref
cat $outdir/test.q0-ADB-VNC.$cleanSuffix.ref $outdir/test.non-q0-ADB-VNC.$cleanSuffix.ref > $outdir/test.$cleanSuffix.ref

cat $testHyp | sed "s/$c1//g" | sed "s/$c2//g" | sed "s/$c3//g" | egrep "(_q0|VNCVMX|ADBVMX)" > $outdir/$modelSuffix.test.q0-ADB-VNC.$cleanSuffix.hyp
cat $testHyp | sed "s/$c1//g" | sed "s/$c2//g" | sed "s/$c3//g" | egrep -v "(_q0|VNCVMX|ADBVMX)" > $outdir/$modelSuffix.test.non-q0-ADB-VNC.$cleanSuffix.hyp
cat $outdir/$modelSuffix.test.q0-ADB-VNC.$cleanSuffix.hyp $outdir/$modelSuffix.test.non-q0-ADB-VNC.$cleanSuffix.hyp > $outdir/$modelSuffix.test.$cleanSuffix.hyp
compute-wer --text --mode=present ark:$outdir/test.$cleanSuffix.ref ark:$outdir/$modelSuffix.test.$cleanSuffix.hyp > /tmp/wer
get_wer ; ((i+=1)) ; matrix[3,$i]=$wer; matrix[1,$i]=$wrdCnt; matrix[2,$i]=$sentCnt;
compute-wer --text --mode=present ark:$outdir/test.non-q0-ADB-VNC.$cleanSuffix.ref ark:$outdir/$modelSuffix.test.non-q0-ADB-VNC.$cleanSuffix.hyp > /tmp/wer
get_wer ; ((i+=1)) ; matrix[3,$i]=$wer; matrix[1,$i]=$wrdCnt; matrix[2,$i]=$sentCnt;
compute-wer --text --mode=present ark:$outdir/test.q0-ADB-VNC.$cleanSuffix.ref ark:$outdir/$modelSuffix.test.q0-ADB-VNC.$cleanSuffix.hyp > /tmp/wer
get_wer ; ((i+=1)) ; matrix[3,$i]=$wer; matrix[1,$i]=$wrdCnt; matrix[2,$i]=$sentCnt;

###------------WER on dialogs with new dialog without q0 and b------------###
echo "-------non-b-q0-ADB-VNC---------"
cat $testRef | sed "s/$c1//g" | sed "s/$c2//g" | sed "s/$c3//g" | egrep -v "m[0-9]+b" | egrep -v "(_q0|VNCVMX|ADBVMX)" > $outdir/test.non-b-q0-ADB-VNC.$cleanSuffix.ref

cat $testHyp | sed "s/$c1//g" | sed "s/$c2//g" | sed "s/$c3//g" | egrep -v "m[0-9]+b" | egrep -v "(_q0|VNCVMX|ADBVMX)" > $outdir/$modelSuffix.test.non-b-q0-ADB-VNC.$cleanSuffix.hyp
compute-wer --text --mode=present ark:$outdir/test.non-b-q0-ADB-VNC.$cleanSuffix.ref ark:$outdir/$modelSuffix.test.non-b-q0-ADB-VNC.$cleanSuffix.hyp > /tmp/wer
get_wer; ((i+=1)); matrix[3,$i]=$wer; matrix[1,$i]=$wrdCnt; matrix[2,$i]=$sentCnt;

echo ""
num_rows=3
num_columns=4
f2="|%14s "
for ((i=1;i<=num_rows;i++)) do
	for ((j=1;j<=num_columns;j++)) do
        printf "$f2" ${matrix[$i,$j]}
    done
    echo " |"
done





