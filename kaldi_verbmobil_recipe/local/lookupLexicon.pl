#!/usr/bin/perl

# Copyright © 2019 INRIA (Imran Sheikh)
# Apache 2.0  (http://www.apache.org/licenses/LICENSE-2.0)

if($#ARGV != 1){
	print STDERR "USAGE: $0 <ovv-marked_text_trascriptions> <cmudict>\n";
	exit(0);
}

my $WFTHRESHOLD = 2;

my %lexicon;
open my $FH, "<", $ARGV[1] or die "can't read open '$ARGV[1]': $OS_ERROR";
while (my $line = <$FH>) {
	$line =~ s/\s+$//;
	$line = lc($line);
	my ($wrd, $lex) = split(/\s+/, $line, 2);
	$wrd =~ s/\(\d+\)$//;
	$lex =~ s/\d//g;
	if(exists($lexicon{$wrd})){
		$lexicon{$wrd} = $lexicon{$wrd}."|".$lex;
	}else{
		$lexicon{$wrd} = $lex;
	}
}
close($FH);

my %wfreq;
open my $FH, "<", $ARGV[0] or die "can't read open '$ARGV[0]': $OS_ERROR";
while (my $line = <$FH>) {
	$line =~ s/\s+$//;
	my ($id, $txt) = split(/\s+/, $line, 2);
	my @wrds = split(/\s+/, $txt);
	foreach my $wrd(@wrds){
		if(exists($wfreq{$wrd})){
			$wfreq{$wrd} += 1;
		}else{
			$wfreq{$wrd} = 1;
		}
	}
}
close($FH);

foreach my $key (sort (keys(%wfreq))) {
	if($key eq "<unk>"){
		print "<unk> oov\n";
	}elsif($key eq "[hes]"){
		print "[hes] hes\n";
	}elsif($key =~ m/\[noise\d\]/){
		my $pron = $key;
		$pron =~ s/^\[//;
		$pron =~ s/\]$//;
		print "$key $pron\n";
	}else{
		if(exists($lexicon{$key})){
			my @prons = split(/\|/, $lexicon{$key});
			foreach my $pron (@prons){
				print "$key $pron\n";
			}
		}else{
			print "$key <add_manually>\n";
		}
	}
}
