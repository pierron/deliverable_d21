The text transformer tool helps to:
* identify sensitive words or named entities in a dialog conversation
* Text transformation of sensitive words by words of the same-type or placeholders

----

## Requirements
Python 2.7 or 3 with following packages:
* numpy
* pandas
* flair

## Usage
### Identification of named entities in a dialog conversation
Given an annotated text file(s) in a CoNLL format, split them into training, development and text split using: 
> python preprocess.py

The named entity recognition (NER) is trained using [Flair] (https://github.com/zalandoresearch/flair) that is based on BiLSTM-CRF model and word features are obtained from Glove embeddings. 

To train the NER model, use:
> python train_ner.py ---input_dir training_data_dir ---output_dir model_output_dir

`training_data_dir`  the directory consisting of the training, development and test data in '.tsv' extension

`model_output_dir`  the directory where the model output, and training log is stored
 
To test the NER model, use:
> python test_ner.py 


### Text transformation of sensitive words by words of the same-type or placeholders
1) create training dataset to evaluate the impact of different text transformation strategies, we consider 5 ways of transforming sensitive words/ named entities (PER, LOC, ORG, DATE and TIME): 
* replace all single words labeled as named entities with a placeholder 
* replace all multi-word expressions labeled as named entities with a placeholder
* replace all single words labeled as named entities with the same-type word
* replace all multi-word expressions labeled as named entities with same-type word 
* replace all multi-word expressions labeled as named entities with a multi-word expression of the same-type 

> python create_privacy_transformed_data.py

2) Demo of text transformation, that accepts a sentence from the command line, automatically identify the sensitive words, and replace each single word labeled as named entity with another word of the same-type 

> python demo_text_transformation.py

> Enter a sample sentence: Mark Smith is going to London in April

> Tagged sentence:  Mark <B-PER> Smith <B-PER> is going to London <B-LOC> in April <B-DATE>

> Transformed sentence:  Bottermaker Norbert is going to London in Thursday 
