import os
import sys
import spacy
import pandas as pd
import string

nlp = spacy.load("en_core_web_sm")

def test_data_for_spacy(data_list, dir):
    
    comb_texts = []
    for f_file in data_list:
        with open(dir + f_file) as f:
            file_text = f.readlines()

        for k, line in enumerate(file_text):
            word, ne = line.split()
            comb_texts.append(word)
    
    convers_text = ' '.join(comb_texts)
    
    # Spacy annotation
    doc = nlp(convers_text)

    entity_names = {}
    for entity in doc.ents:
        # print(entity.text, entity.label_)
        entity_splits = entity.text.split()
        if len(entity_splits) > 1:
            for ent in entity_splits:
                entity_names[ent] = entity.label_
        else:
            entity_names[entity.text] = entity.label_

    tokens = convers_text.split()

    cons_NEs = ['PERSON', 'ORG', 'LOC', 'GPE', 'FAC', 'DATE', 'TIME',
                'PERCENT', 'MONEY', 'QUANTITY', 'ORDINAL', 'CARDINAL']
    token_nes = []
    for token in tokens:
        if token in entity_names and entity_names[token] in cons_NEs:
            ne = entity_names[token]
            if ne == 'PERSON':
                ne = 'PER'
            elif ne in ['LOC', 'GPE', 'FAC']:
                ne = 'LOC'
            elif ne in ['PERCENT', 'MONEY', 'QUANTITY', 'ORDINAL', 'CARDINAL']:
                ne = 'O'
        
            word = token.strip()
            # f_write.write(token+ ','+ ne+ '\n')
        else:
            word = token.strip()
            ne = 'O'
        token_nes.append([word, ne])
                    
    t_data = []
    for k, token_n_ne in enumerate(token_nes):
        token, ne = token_n_ne[0], token_n_ne[1]
        prev_ne = 'O'
        if k > 0:
            prev_ne = token_nes[k-1][1] 
            
        if token == '~#' or token == '.':
            t_data.append(['', ''])
        else:
            new_ne = ne
            if new_ne != 'O':
                ne = 'B-'+new_ne
                if new_ne == prev_ne:
                    ne = 'I-'+new_ne
                    
            t_data.append([token, ne])
        
    return t_data
    
    
def split_sentences_by_period(data_list, dir, islower=False, no_punctuations=False):
    t_data = []
    u = 0
    
    punctuations = set(string.punctuation)
    for f_file in data_list:
        with open(dir + f_file) as f:
            file_text = f.readlines()

        for k, line in enumerate(file_text):
            word, ne = line.split()
            if k > 0:
                prev_ne = file_text[k-1].split()[1]
            else:
                prev_ne = 'O'
         
            if word == '~#' or word == '.' or word == '#':
                t_data.append(['', ''])
                if word == '~#' or word == '#': u+=1
            else:
                if ne == 'TIMR':
                    ne = 'TIME'
                elif ne  == 'NORP' or ne == 'NUMBER' or ne == 'o':
                    ne = 'O'

                new_ne = ne
                if new_ne != 'O':
                    ne = 'B-'+new_ne
                    if new_ne == prev_ne:
                        ne = 'I-'+new_ne
                        
                if islower:
                    word = word.lower()
                    
                if word in punctuations:
                    if not no_punctuations:
                        t_data.append([word, ne])
                else:    
                    t_data.append([word, ne])

    print('# sentences', u)
    return t_data


# Divide Verbmobil data with gold annotation into train and test splits
def create_train_test_data_split(output_dir, isLower=False, no_punctuations=False):
    raw_dir = '../data/raw/spacy_anea_annotations/'
    other_dir = '../data/raw/figure8_annotations/'
    dir_files = os.listdir(raw_dir)
    print(len(dir_files))
    train_list = dir_files[:388]
    valid_list = os.listdir(other_dir)
    test_list = dir_files[388:]

    train_data  = split_sentences_by_period(train_list, raw_dir, isLower, no_punctuations)
    valid_data = split_sentences_by_period(valid_list, other_dir, isLower,no_punctuations)
    test_data = split_sentences_by_period(test_list, raw_dir, isLower, no_punctuations)
    spacy_test_data = test_data_for_spacy(test_list, raw_dir)


    train_df = pd.DataFrame(train_data)
    train_df.to_csv(output_dir+'train.tsv', sep='\t', header=False, index=False)

    valid_df = pd.DataFrame(valid_data)
    valid_df.to_csv(output_dir+'valid.tsv', sep='\t', header=False, index=False)
    
    
    test_df = pd.DataFrame(test_data)
    test_df.to_csv(output_dir+'test.tsv', sep='\t', header=False, index=False)

    spacy_test_df = pd.DataFrame(spacy_test_data)
    spacy_test_df.to_csv(output_dir+'spacy_test.tsv', sep='\t', header=False, index=False)
    
    
    
    ner_data = pd.read_csv(output_dir+'train.tsv', sep="\t", header=None, encoding="utf-8")
    ner_data.columns = ["token", "ne"]

    # Explore the distribution of NE tags in the dataset
    tag_distribution = ner_data.groupby("ne").size().reset_index(name='counts')
    print(tag_distribution)

    

    return train_data, valid_data, test_data

if __name__ == '__main__':
    output_dir = '../data/training/bio_uncased/'
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)
    train_data, valid_data, test_data = create_train_test_data_split(output_dir, isLower = True)

    output_dir = '../data/training/bio_cased/'
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)
    train_data, valid_data, test_data = create_train_test_data_split(output_dir, isLower = False)
    
    output_dir = '../data/training/bio_uncased_nopunct/'
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)
    train_data, valid_data, test_data = create_train_test_data_split(output_dir, isLower = True, no_punctuations=True)

    output_dir = '../data/training/bio_cased_nopunct/'
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)
    train_data, valid_data, test_data = create_train_test_data_split(output_dir, isLower = False, no_punctuations=True)

    
